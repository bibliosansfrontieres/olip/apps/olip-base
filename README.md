# olip-base

A base image for all your OLIP packaging needs.

## Documentation

All OLIP Apps images are based on `olip-base`.

## Content management

The `olip-api` container writes install/uninstall/upgrade requests
to an App's `/data/content/queue/data.db` queue file.

In the App container, the `olip_queue_management.py` script
checks whether the queue file has some content to handle.

If it find any, it will run the appropriate script, providing two arguments:

* `$1`: `content_id`
* `$2`: `download_path`

## scripts

The `install_content.sh`, `uninstall_content.sh` and `update_content.sh`
are meant to be overriden by specific instructions for every App.

For instance, a Kiwix image will use the `kiwix-manage` binary.

## Developement tools

To be done.
